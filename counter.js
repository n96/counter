
var counter = 0;

var starter = false;

// checking if its clicked on button and started

$(".big").click(function() {

    if(!starter) {
      $("#count").toggleClass("invisible").text(counter);
      starter = true;
      dec();
      inc();
      resetCounter();
    }

})

// decreasing counter

function dec() {
    $("#dec").click(function() {
      counter--;
      $("#count").text(counter);
      var decSound = new Audio("sounds/decrease.mp3");
      decSound.play();
      minusCheck();
    })
}

// increasing counter

function inc() {
    $("#inc").click(function() {
      counter++;
      $("#count").text(counter);
      var incSound = new Audio("sounds/increase.mp3");
      incSound.play();
      minusCheck();
    })
}

// reseting counter to 0

function resetCounter() {
    $("#reset").click(function() {
      counter = 0;
      $("#count").text(counter);
      var resetSound = new Audio("sounds/reset.mp3");
      resetSound.play();
      minusCheck();
    })
}

// function to check if counter is below 0

function minusCheck() {
  if(counter < 0) {
    $("#count").addClass("redNumber");
  } else {
    $("#count").removeClass("redNumber");
  }
}
